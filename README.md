# Códigos-fonte de Eduardo Camargo de Siqueira

👨‍🏫 Professor de Informática no [IFB - Campus São Sebastião](https://www.ifb.edu.br/saosebastiao) desde Novembro de 2021. Formado em Ciência da Computação, com mestrado e doutorado na área de **Modelagem Matemática e Computacional**. 
🔬 Pesquisador no [AILAB - UnB](https://ailab.unb.br/) e no [CEFET-MG](https://www.cefetmg.br/), onde atua em projetos voltados a **Machine Learning** e **Inteligência Computacional**. 
💻 Também possui experiência em Pesquisa Operacional, Métodos Numéricos Computacionais e Programação C/C++, PHP e Python.

## 🔗 Links

- [📄 Currículo Lattes](http://lattes.cnpq.br/1383558830607731)
- [💻 Códigos-fonte](https://gitlab.com/ecsiqueira35)
- [✉️ Email](mailto:eduardo.siqueira@ifb.edu.br)
- [📹 YouTube](https://www.youtube.com/channel/UCSopjE1rnNcg175Wb0acHzw)
- [📸 Instagram](http://instagram.com/profduca)

## 👨‍🏫  Disciplinas

- 💻 Informática
- 🦾 Inteligência Artificial
- </> Programação Web
- 📱 Programação Móvel
- 📄 Projeto Integrador I

## 📚 Linhas de Pesquisa

- 🤖 **Machine Learning**:
Transformando dados em conhecimento, de forma inteligente e eficiente.

- 📊 **Inteligência Computacional para Otimização**: 
Maximizando resultados e minimizando esforços.

- 🔍 **Otimização Combinatória**:
Desvendando eficiência na simplicidade das escolhas.

- 🖥️ **Informática na Educação**:
Capacitando alunos e educadores a explorarem um novo horizonte de possibilidades educativas.

## 📄 Licença

A permissão para de usar, copiar, modificar, mesclar, publicar, distribuir, é concedida, gratuitamente, a qualquer pessoa, desde que seja para fins pessoais e/ou didáticos. **A venda total ou parcial deste software é proibida!**

---
⌨️ por [Duca Siqueira](https://duca.pro.br) 😊.
