# Repositório da Disciplina de Informática

Este repositório contém códigos em Python para diversos projetos da disciplina de Informática, organizados em dois diretórios principais: `data` e `notebooks`.

## 📁 Datasets

O diretório `data` contém conjuntos de dados utilizados nos projetos. Esses datasets podem ser utilizados para treinamento, teste ou análise dos modelos e algoritmos implementados nos notebooks.

## 💻 Notebooks

O diretório `notebooks` contém Notebooks com códigos, análises e visualizações dos projetos desenvolvidos em Python. Os notebooks são uma forma interativa e documentada de explorar os dados e os resultados dos modelos implementados.

## Plano de Ensino
### Metodologia

    🗣️ Exposição de conteúdo no formato aulas dialogadas;
    🖥️ Aplicação de exercícios práticos em laboratório;
    💻 Atividades em sala de aula virtual e em ferramentas próprias;


### Recursos

    🏫 Sala de aula com quadro e pincel;
    🖥️ Laboratório de informática com projetor;
    📱 Computador ou celular com internet;

### Bases Tecnológicas

    📦 Características dos pacotes de escritório;
    📝 Redação oficial usando editores de texto;
    📊 Cálculos e operações em planilhas eletrônicas;
    📈 Seminários e ideias demonstrando o uso de apresentações eletrônicas e demais ferramentas;
    🔒 Ferramentas e estratégias de segurança na Internet.

### Habilidades

    ✍️ Utilizar ferramentas de edição de texto, planilhas e apresentação.
    📝 Reconhecer e construir memorandos e ofícios;
    🖼️ Construir documentos de texto utilizando elementos de sumário, tabelas, figuras, etc.
    📧 Fazer mala direta, banners, cartazes e outras tarefas simples com pacote de escritório;
    📊 Identificar as planilhas, realizar cálculos, operações e formatações;
    🖥️ Desenvolver apresentações simples;
    🌐 Utilizar a internet com segurança.

### Competências

    📄 Produzir documentos eletrônicos utilizando as ferramentas necessárias;
    📂 Gerenciar arquivos eletrônicos no computador;
    🛠️ Implementar soluções simples e práticas para análise de dados.

## 📄 Licença

A permissão para de usar, copiar, modificar, mesclar, publicar, distribuir, é concedida, gratuitamente, a qualquer pessoa, desde que seja para fins pessoais e/ou didáticos. A venda total ou parcial deste software é proibida!

---
⌨️ por [Duca Siqueira](https://duca.pro.br) 😊.