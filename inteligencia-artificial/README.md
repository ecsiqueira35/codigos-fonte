# Repositório sobre Inteligência Artificial

Este repositório contém materiais sobre Inteligência Artificial, organizados em três diretórios principais:

📂 aulas – material didático utilizados para ensino e aprendizado.

📂 lista-exercicios – conjunto de exercícios práticos para aprimorar habilidades.

📂 machine-learning – implementações de algoritmos e experimentos em aprendizado de máquina.

## Aulas

- [Aula 01 - Introdução à Inteligência Artificial](https://gitlab.com/ecsiqueira35/codigos-fonte/-/blob/main/inteligencia-artificial/aulas/01-Aula01-IntroIA.pdf?ref_type=heads)
- [Aula 02 - Machine learning](https://gitlab.com/ecsiqueira35/codigos-fonte/-/blob/main/inteligencia-artificial/aulas/02-Aula02-ML.pdf?ref_type=heads)
- Aula 03 - Aprendizado não-supervisionado
- Aula 04 - Deep learning
- Aula 05 - Modelos de linguagem
- Aula 06 - ...
- Aula 07 - ...

## Exercícios

- Lista de exercícios 01
- Lista de exercícios 02
- Lista de exercícios 03
- Lista de exercícios 04

## ❤️ Contribuição

Se você deseja contribuir com novos códigos, notebooks ou datasets para este repositório, sinta-se à vontade para abrir uma pull request. Teremos prazer em revisar suas contribuições. Se você encontrar algum erro ou tiver alguma dúvida, envie um e-mail: eduardo.siqueira@ifb.edu.br. Obrigado por contribuir e espero que você aprenda bastante com este material!

## 📄 Licença

A permissão para de usar, copiar, modificar, mesclar, publicar, distribuir, é concedida, gratuitamente, a qualquer pessoa, desde que seja para fins pessoais e/ou didáticos. A venda total ou parcial deste software é proibida!

---
⌨️ por [Duca Siqueira](https://duca.pro.br) 😊.