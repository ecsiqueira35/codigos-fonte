# Repositório de Códigos em Python

Este repositório contém códigos em Python para diversos projetos de machine learning, organizados em dois diretórios principais: `data` e `notebooks`.

## 📁 Datasets

O diretório `data` contém conjuntos de dados utilizados nos projetos. Esses datasets podem ser utilizados para treinamento, teste ou análise dos modelos e algoritmos implementados nos notebooks.

## 💻 Notebooks

O diretório `notebooks` contém Notebooks com códigos, análises e visualizações dos projetos desenvolvidos em Python. Os notebooks são uma forma interativa e documentada de explorar os dados e os resultados dos modelos implementados.

## ❗ Como utilizar

Para utilizar os códigos e datasets deste repositório, siga os passos abaixo:

1. Clone este repositório em sua máquina local.

2. Acesse os diretórios `data` e `notebooks` para explorar os conjuntos de dados e os notebooks disponíveis.

3. Abra os notebooks em um ambiente virtual para executar os códigos e visualizar os resultados.

## ❤️ Contribuição

Se você deseja contribuir com novos códigos, notebooks ou datasets para este repositório, sinta-se à vontade para abrir uma pull request. Teremos prazer em revisar suas contribuições.

## 📄 Licença

A permissão para de usar, copiar, modificar, mesclar, publicar, distribuir, é concedida, gratuitamente, a qualquer pessoa, desde que seja para fins pessoais e/ou didáticos. A venda total ou parcial deste software é proibida!

---
⌨️ por [Duca Siqueira](https://duca.pro.br) 😊.