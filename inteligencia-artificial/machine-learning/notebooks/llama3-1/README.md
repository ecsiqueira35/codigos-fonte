# 📚 Exemplo de Uso do Ollama

Este diretório contém um notebook de exemplo que demonstra como utilizar a biblioteca `ollama` para interagir com modelos de linguagem.

## 🗂 Estrutura do Diretório

- `experimento-prontuarios.ipynb`: Notebook Jupyter que ilustra o uso da biblioteca `ollama` com exemplos de código e explicações.
- `requirements.txt`: Arquivo de dependências do Python para o projeto.

## 🛠 Pré-requisitos

Certifique-se de que você tem o Python e o Visual Studio Code (VS Code) instalados em seu ambiente.

## 🚀 Instalação

1. **Clone o Repositório** 🧩

   Se ainda não tiver o diretório, clone o repositório usando:

   ```bash
   git clone https://gitlab.com/ecsiqueira35/codigos-fonte.git
   ```

2. **Navegue até o Diretório** 🏠

   Entre no diretório contendo o notebook:

   ```bash
   cd <nome-do-diretorio>
   ```

3. **Instale as Dependências** 📦

   Utilize o `requirements.txt` para instalar todas as dependências necessárias. Execute o seguinte comando:

   ```bash
   pip install -r requirements.txt
   ```

4. **Abra o VS Code** 💻

   Abra o Visual Studio Code e navegue até o diretório do projeto. Você pode fazer isso diretamente do terminal com:

   ```bash
   code .
   ```

5. **Abra o Notebook** 📑

   No VS Code, abra o arquivo `experimento-prontuarios.ipynb`. O VS Code fornece suporte integrado para notebooks Jupyter, permitindo que você execute as células diretamente na interface.

6. **Execute as Células** ▶️

   Siga as instruções no notebook e execute as células para ver os exemplos em ação.

## 🤝 Contribuições

Se você deseja contribuir para este projeto, sinta-se à vontade para abrir uma issue ou enviar um pull request.

## 📄 Licença

A permissão para de usar, copiar, modificar, mesclar, publicar, distribuir, é concedida, gratuitamente, a qualquer pessoa, desde que seja para fins pessoais e/ou didáticos. **A venda total ou parcial deste software é proibida!**

---
⌨️ por [Duca Siqueira](https://duca.pro.br) 😊.
