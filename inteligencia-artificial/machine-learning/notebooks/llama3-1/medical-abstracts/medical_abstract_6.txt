Diagnóstico pré-operatório: Fratura diafisária de fêmur esquerdo
Procedimento realizado: Osteossíntese de fêmur com haste intramedular bloqueada
Anestesia: Raquianestesia com sedação
Equipe:

    Cirurgião: Dr. Gustavo Menezes
    Anestesiologista: Dr. Ricardo Faria
    Assistente: Dr. André Santos
    Enfermeira circulante: Juliana Lopes

Descrição técnica do procedimento:
Paciente posicionado em decúbito dorsal sobre mesa ortopédica com tração controlada. Após antissepsia e colocação dos campos estéreis, foi realizada incisão longitudinal na região proximal do fêmur. Introdução de guia intramedular, seguida pela passagem da haste intramedular até o foco da fratura.

Haste intramedular bloqueada com parafusos distais e proximais guiados por fluoroscopia. Controle de hemostasia feito com eletrocautério. Lavagem abundante com solução salina antes do fechamento da incisão com suturas absorvíveis.

Material utilizado:
Haste intramedular bloqueada, parafusos, fluoroscópio, solução salina, eletrocautério, fios de sutura absorvíveis.

Tempo operatório: 2 horas e 45 minutos.

Intercorrências: Nenhuma.

Destino da paciente: Encaminhado à sala de recuperação anestésica para observação por 3 horas, com controle rigoroso de sinais vitais. Posteriormente, transferido para a unidade de internação ortopédica.

Evolução imediata pós-operatória:
Paciente sem queixas relevantes, com dor controlada por analgesia venosa contínua. Mobilização passiva do membro inferior iniciada pela equipe de fisioterapia no primeiro dia pós-operatório. Curativos limpos e secos, sem sinais de complicações locais. Alta hospitalar planejada após 72 horas, com acompanhamento ambulatorial.