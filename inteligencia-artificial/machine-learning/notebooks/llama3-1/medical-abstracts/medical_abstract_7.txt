Diagnóstico pré-operatório: Cisto ovariano à direita
Procedimento realizado: Cistectomia ovariana laparoscópica
Anestesia: Geral
Equipe:

    Cirurgião: Dr. Antônio Farias
    Anestesiologista: Dra. Paula Lima
    Assistente: Dr. Rafael Moreira
    Enfermeira circulante: Sandra Rocha

Descrição técnica do procedimento:
Paciente posicionada em litotomia com leve trendelemburg. Após antissepsia e colocação de campos estéreis, foi realizada incisão periumbilical para introdução do primeiro trocárter e insuflação abdominal com CO₂. Dois trocárteres adicionais foram posicionados em local apropriado.

O ovário direito foi identificado, evidenciando o cisto de paredes finas. A dissecção cuidadosa foi realizada com pinças laparoscópicas e o uso de energia bipolar, preservando ao máximo o tecido ovariano. O cisto foi aspirado e removido em sua totalidade. Irrigação da cavidade com solução salina, seguida de revisão da hemostasia com eletrocautério. Portais fechados com sutura absorvível.

Material utilizado:
Trocárteres, pinças laparoscópicas, energia bipolar, eletrocautério, solução salina, fios de sutura absorvíveis.

Tempo operatório: 1 hora e 50 minutos.

Intercorrências: Nenhuma.

Destino da paciente: Encaminhada à sala de recuperação pós-anestésica para monitoramento por 2 horas, com alta para a enfermaria após estabilização dos sinais vitais.

Evolução imediata pós-operatória:
Paciente evoluiu bem, com dor controlada por analgesia venosa. Mantida em observação por 24 horas, com curativos limpos e secos, sem sinais de complicações. Alta hospitalar programada com orientações de repouso relativo e acompanhamento ambulatorial em 10 dias.