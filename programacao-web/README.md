# Repositório da Disciplina de Programação Web

Este repositório contém códigos em HTML/CSS e Javascript para diversos projetos desenvolvidos durante a disciplina de Programação Web.

## Plano de Ensino

### Metodologia

    🗣️ Exposição de conteúdo no formato aulas dialogadas;
    🖥️ Aplicação de exercícios práticos em laboratório;
    💻 Atividades em sala de aula virtual e em ferramentas próprias;


### Recursos

    🏫 Sala de aula com quadro e pincel;
    🖥️ Laboratório de informática com projetor;
    📱 Computador ou celular com internet;

### Bases Tecnológicas

    🌐 Introdução a Linguagem de Programação Web para servidor.
    📑 Componentes das linguagens para desenvolvimento Web, tipos e escopo das variáveis, controle de seção e cookies.
    🔧 Tratamento de erros em formulários, passagem de parâmetros e validação de entrada de dados.
    ➕ Operadores aritméticos, relacionais e lógicos, comandos de controle de fluxo, condicional e laços de repetição.
    🔄 Funções de reaproveitamento de código, elaboração de bibliotecas de funções, uso de funções prontas como criptografia, envio de e-mail e upload de arquivos.
    🏗️ Estrutura de aplicações em camadas (MVC).

### Habilidades

    🖥️ Aplicar as técnicas de programação para a internet no desenvolvimento de páginas web.
    📝 Criar formulários para websites com conexões com bancos de dados
    🔧 Elaborar e configurar arquiteturas, serviços e funções de servidores web
    🧩 Entender e aplicar a técnica de orientação a objetos no desenvolvimento web e aplicar técnicas de modularização
    🔒 Efetuar transações de dados em sistemas web de forma segura e utilizar conceitos de segurança em sistemas para internet.

### Competências

    🌍 Produzir sistemas web para o ensino e/ou escolas;
    🏗️ Gerenciar arquitetura de aplicações web;
    💡 Implementar software para web baseado nos princípios de usabilidade.

## 📄 Licença

A permissão para de usar, copiar, modificar, mesclar, publicar, distribuir, é concedida, gratuitamente, a qualquer pessoa, desde que seja para fins pessoais e/ou didáticos. **A venda total ou parcial deste software é proibida!**

---
⌨️ por [Duca Siqueira](https://duca.pro.br) 😊.