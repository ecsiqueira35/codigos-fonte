# Educast - Aprendizado todo dia

Após a pandemia, muitas pessoas começaram a ouvir programas de áudio e o público que consome esse tipo de conteúdo internet saltou consideravelmente nos últimos anos. Assim, como também houve mudanças drásticas na educação e na forma como professores e alunos se relacionam e compartilham experiências. Nesse sentido, temos a ideia de criar uma plataforma de agregação de conteúdo educacional em formato de podcast, o Educast.

Um podcast é um programa disponibilizado em formato digital para download pela Internet em formato de áudio. Aplicativos de streaming e serviços de podcasting fornecem uma maneira conveniente e integrada de gerenciar uma fila de consumo pessoal em muitas fontes de podcast e dispositivos de reprodução. Também existem mecanismos de busca de podcast, que ajudam os usuários a encontrar e compartilhar episódios de podcast.

## 🚀 Começando

Com este trabalho exercitamos as seguintes ferramentas:

* HTML5;
* CSS3 e Bootstrap;
* Javascript.

[Clique aqui](https://wooded-basil-693.notion.site/Educast-Aprendizado-todo-dia-b1115009510142729358098481f0822f) para mais informações de como foi feito.

## 📄 Licença

A permissão para de usar, copiar, modificar, mesclar, publicar, distribuir, é concedida, gratuitamente, a qualquer pessoa, desde que seja para fins pessoais e/ou didáticos. A venda total ou parcial deste software é proibida!

---
⌨️ por [Duca Siqueira](https://duca.pro.br) 😊 durante a disciplina de Programação Web do curso técnico em Desenvolvimento de Sistemas Educacionais (IFB Campus São Sebastião).