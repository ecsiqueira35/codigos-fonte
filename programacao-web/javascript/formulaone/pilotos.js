data = [
    {
        "pos": 1,
        "imagem": "imagens/verstappen.png",
        "nome": "Max Verstappen",
        "pontos": 454,
        "equipe": "Red Bull Racing",
        "numero": 1,
        "pais": "Países Baixos"        
    },

    {
        "pos": 2,
        "imagem": "imagens/leclerc.png",
        "nome": "Charles Leclerc",
        "pontos": 308,
        "equipe": "Ferrari",
        "numero": 16,
        "pais": "Mônaco"        
    },

    {
        "pos": 3,
        "imagem": "imagens/perez.png",
        "nome": "Sergio Perez",
        "pontos": 305,
        "equipe": "Red Bull Racing",
        "numero": 11,
        "pais": "México"        
    },

    {
        "pos": 4,
        "imagem": "imagens/russel.png",
        "nome": "George Russel",
        "pontos": 275,
        "equipe": "Mercedes",
        "numero": 63,
        "pais": "Inglaterra"        
    },


    {
        "pos": 5,
        "imagem": "imagens/sainz.png",
        "nome": "Carlos Sainz",
        "pontos": 246,
        "equipe": "Ferrari",
        "numero": 55,
        "pais": "Espanha"        
    },

    {
        "pos": 6,
        "imagem": "imagens/hamilton.png",
        "nome": "Lewis Hamilton",
        "pontos": 240,
        "equipe": "Mercedes",
        "numero": 44,
        "pais": "Inglaterra"        
    },

    {
        "pos": 7,
        "imagem": "imagens/norris.png",
        "nome": "Lando Norris",
        "pontos": 122,
        "equipe": "McLaren",
        "numero": 4,
        "pais": "Inglaterra"        
    },

    {
        "pos": 8,
        "imagem": "imagens/ocon.png",
        "nome": "Esteban Ocon",
        "pontos": 92,
        "equipe": "Alpine",
        "numero": 31,
        "pais": "França"        
    },

    {
        "pos": 9,
        "imagem": "imagens/alonso.png",
        "nome": "Fernando Alonso",
        "pontos": 81,
        "equipe": "Alpine",
        "numero": 14,
        "pais": "Espanha"        
    },


    {
        "pos": 10,
        "imagem": "imagens/bottas.png",
        "nome": "Valtteri Bottas",
        "pontos": 49,
        "equipe": "Alfa Romeo",
        "numero": 77,
        "pais": "Finlândia"        
    }
    
];