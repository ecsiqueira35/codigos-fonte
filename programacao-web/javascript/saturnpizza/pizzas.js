pizzas = [
    {
        "imagem": "imagens/calabresa.jpg",
        "nome": "Calabresa",
        "ingredientes": "muçarela, calabresa e cebola",
        "preco": 30.90        
    }, 
    {
        "imagem": "imagens/portuguesa.jpg",
        "nome": "Portuguesa",
        "ingredientes": "presunto, ovos, muçarela e ervilha",
        "preco": 30.90        
    },
    {
        "imagem": "imagens/atum.jpg",
        "nome": "Atum",
        "ingredientes": "atum sólido e cebola",
        "preco": 30.90        
    },
    {
        "imagem": "imagens/baiana.jpg",
        "nome": "Baiana",
        "ingredientes": "calabresa moída, ovos, pimenta, cebola e parmesão",
        "preco": 30.90        
    },
    {
        "imagem": "imagens/brocolis.jpg",
        "nome": "Brócolis",
        "ingredientes": "brócolis, bacon crocante e catupiry",
        "preco": 30.90        
    }  
    
];