alunos = [
  {
    "nome": "Ana Beatriz Santos",
    "data_nascimento": "2009-08-07",
    "sexo": "Feminino",
    "filiacao": {
      "pai": "Roberto Santos",
      "mae": "Mariana Oliveira"
    }
  },
  {
    "nome": "Ana Luiza Oliveira",
    "data_nascimento": "2009-09-22",
    "sexo": "Feminino",
    "filiacao": {
      "pai": "Fernando Oliveira",
      "mae": "Patricia Costa"
    }
  },
  {
    "nome": "Bruno Castro",
    "data_nascimento": "2009-04-01",
    "sexo": "Masculino",
    "filiacao": {
      "pai": "Vinicius Castro",
      "mae": "Renata Oliveira"
    }
  },
  {
    "nome": "Camila Almeida",
    "data_nascimento": "2009-10-10",
    "sexo": "Feminino",
    "filiacao": {
      "pai": "Lucas Almeida",
      "mae": "Isabela Castro"
    }
  },  
  {
    "nome": "Carolina Pereira",
    "data_nascimento": "2009-11-11",
    "sexo": "Feminino",
    "filiacao": {
      "pai": "Marcos Pereira",
      "mae": "Fernanda Silva"
    }
  },
  {
    "nome": "Diego Andrade",
    "data_nascimento": "2009-07-19",
    "sexo": "Masculino",
    "filiacao": {
    "pai": "Gustavo Andrade",
    "mae": "Carla Martins"
    }
  },
  {
    "nome": "Guilherme Costa",
    "data_nascimento": "2009-01-02",
    "sexo": "Masculino",
    "filiacao": {
      "pai": "Luiz Costa",
      "mae": "Renata Almeida"
    }
  },
  {
    "nome": "João Santos",
    "data_nascimento": "2009-06-02",
    "sexo": "Masculino",
    "filiacao": {
      "pai": "Pedro Santos",
      "mae": "Carla Oliveira"
    }
  },
  {
    "nome": "Luana Oliveira",
    "data_nascimento": "2009-12-24",
    "sexo": "Feminino",
    "filiacao": {
      "pai": "Thiago Oliveira",
      "mae": "Monique Andrade"
    }
  },
  {
    "nome": "Lucas Souza",
    "data_nascimento": "2009-03-15",
    "sexo": "Masculino",
    "filiacao": {
      "pai": "Ricardo Souza",
      "mae": "Juliana Pereira"
    }
  },
  {
    "nome": "Maria da Silva",
    "data_nascimento": "2009-02-14",
    "sexo": "Feminino",
    "filiacao": {
      "pai": "José da Silva",
      "mae": "Ana Souza"
    }
  },
  {
    "nome": "Pedro Henrique Silva",
    "data_nascimento": "2009-05-28",
    "sexo": "Masculino",
    "filiacao": {
      "pai": "Paulo Silva",
      "mae": "Camila Santos"
    }
  }      
]  